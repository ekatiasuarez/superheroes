import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HeroFormComponent } from './components/hero-form/hero-form.component';
import { HeroesComponent } from './components/heroes/heroes.component';

const routes: Routes = [
  {
    path: 'heroes',
    component: HeroesComponent
  },
  {
    path: 'heroes/create',
    component: HeroFormComponent
  },
  {
    path: 'heroes/:id/edit',
    component: HeroFormComponent
  },
  {
    /** Redirect to initial page */
    path: '', redirectTo: '/heroes', pathMatch: 'full'
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
