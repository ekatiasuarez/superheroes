import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import { MatStepper } from '@angular/material/stepper';
import { ActivatedRoute, Router } from '@angular/router';

import { HeroModel } from '../../models/hero.model';
import { HeroService } from '../../services/hero.service';
import { MyErrorStateMatcher } from '../../utils/error-state-matcher';

@Component({
    selector: 'hero-form',
    templateUrl: './hero-form.component.html',
    styleUrls: ['./hero-form.component.sass']
})
export class HeroFormComponent implements OnInit {

    public hero: HeroModel;
    public editMode: boolean = false;
    public heroForm: FormGroup;
    public matcher = new MyErrorStateMatcher();
    public editable: boolean = false;
    public creationSuccess: boolean = false;

    constructor(private formBuilder: FormBuilder,
        private heroService: HeroService,
        private route: ActivatedRoute,
        private _router: Router) {
        // Need empty constructor for dependencies
    }

    ngOnInit(): void {
        this.route.params.subscribe(params => {
            if (params['id']){
                this.heroService.getHeroById(parseInt(params['id'])).then((heroResult) => {
                    this.hero = heroResult;
                    this.editMode = true;
                    this.createForm();
                // Usually catch opens a dialog with an error or something not console.log
                }).catch(error => console.log(`Error ocurred on getHeroById ${error}`));
            } else {
                this.createForm();
            }
        });
    }

    createForm(): void {
        this.heroForm = this.formBuilder.group({
            dataStep: this.formBuilder.group({
                name: [this.hero ? this.hero.name : '', [Validators.required, Validators.minLength(4)]],
                power: [this.hero ? this.hero.power : '', Validators.required],
                active: this.hero ? this.hero.active : true
            })
        });
    }

    setActiveStatus(event: MatSlideToggleChange): void {
        this.heroDataStepForm.controls.active.setValue(event.checked);
    }

    createHero(stepper: MatStepper): void {
        const data = {
            name: this.heroDataStepForm.controls.name.value,
            power: this.heroDataStepForm.controls.power.value,
            active: this.heroDataStepForm.controls.active.value
        };
        this.heroService.createHero(data).then(() => {
            this.handleSuccess(stepper);
        }).catch((error => {
            console.log(`Error ocurred on getHeroes ${error}`);
            this.handleError(stepper);
        }));
        
    }

    editHero(stepper: MatStepper): void {
        const data = {
            id: this.hero.id,
            name: this.heroDataStepForm.controls.name.value,
            power: this.heroDataStepForm.controls.power.value,
            active: this.heroDataStepForm.controls.active.value
        };
        this.heroService.updateHero(data, this.hero.id).then(() => {
            this.handleSuccess(stepper);
        }).catch((error => {
            console.log(`Error ocurred on getHeroes ${error}`);
            this.handleError(stepper);
        }));
    }

    handleSuccess(stepper: MatStepper) {
        this.editable = false;
        this.creationSuccess = true;
        stepper.next();
    }

    handleError(stepper: MatStepper) {
        this.editable = true;
        this.creationSuccess = false;
        stepper.next();
    }

    backToList(): void {
        this._router.navigate(['heroes']);
    }

    get heroDataStepForm(): FormGroup {
        return this.heroForm.controls.dataStep as FormGroup;
    }
}
