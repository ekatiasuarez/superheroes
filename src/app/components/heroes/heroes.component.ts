import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';

import { HeroModel } from '../../models/hero.model';
import { HeroService } from '../../services/hero.service';
import { DialogConfirmComponent, DialogData } from '../dialog/dialog-confirm.component';

@Component({
    selector: 'heroes',
    templateUrl: './heroes.component.html',
    styleUrls: ['./heroes.component.sass']
})
export class HeroesComponent implements OnInit, AfterViewInit {

    public heroesList: Array<HeroModel> = [];
    public heroesSource: MatTableDataSource<any> = new MatTableDataSource();
    public displayedColumns: string[] = ['id', 'name', 'power', 'actions'];

    public searchForm: FormGroup;
    public noResultsFound: boolean = false;

    private delayTimer: any;

    public deletedHero: string = '';

    @ViewChild(MatPaginator) paginator: MatPaginator;

    constructor(private heroService: HeroService,
        private formBuilder: FormBuilder,
        private _router: Router,
        private route: ActivatedRoute,
        public dialog: MatDialog) {
        // Need empty constructor for dependencies
    }

    ngOnInit(): void {
        this.heroService.getHeroes().then((heroes) => {
            this.heroesList = heroes;
            this.heroesSource = new MatTableDataSource<any>(this.heroesList);
            if (this.paginator) this.updatePaginator();
        // Usually catch opens a dialog with an error or something not console.log
        }).catch(error => console.log(`Error ocurred on getHeroes ${error}`));

        this.searchForm = this.formBuilder.group({
            term: ''
        });
    }

    ngAfterViewInit(): void {
        this.updatePaginator();
    }

    filterHeroes() {
        clearTimeout(this.delayTimer);
        this.delayTimer = setTimeout(() => {
            const termValue = this.searchForm.controls.term.value;
            if (termValue.length > 0) {
                this.searchForm.controls.term.disable();
                this.heroService.getFilteredHeroes(termValue).then((heroes) => {
                    this.heroesSource = new MatTableDataSource<any>(heroes);
                    this.updatePaginator();
                    this.searchForm.controls.term.enable();
                }).catch(error => {
                    this.searchForm.controls.term.enable();
                    console.log(`Error ocurred on getFIlteredHeroes ${error}`);
                });
            } else if (this.searchForm.controls.term.dirty) {
                this.heroesSource = new MatTableDataSource<any>(this.heroesList);
                this.updatePaginator();
            }
        }, 1500);
    }

    goToNewHero() {
        this._router.navigate(['create'], { relativeTo: this.route });
    }

    goToEditHero(heroId: number) {
        this._router.navigate([heroId, 'edit'], { relativeTo: this.route });
    }

    openDeleteDialog(hero: HeroModel): void {
        const dialogData: DialogData = { title: 'Delete hero', message: `¿Are you sure you want to eliminate Hero ${hero.name}?`};
        const dialogRef = this.dialog.open(DialogConfirmComponent, { data: dialogData });
        
        dialogRef.afterClosed().subscribe(confirmed => {
            if (!!confirmed) {
                this.heroService.deleteHero(hero.id).then((result) => {
                    if (result) {
                        this.heroesSource = new MatTableDataSource<any>(this.heroesList.filter((heroObj) => heroObj.id != hero.id));
                        this.updatePaginator();
                        this.searchForm.reset();

                        this.showAlert(hero.name);
                    };
                }).catch(error => console.log(`Error ocurred on deleteHero ${error}`));
            }
        })
    }

    showAlert(heroName: string) {
        this.deletedHero = heroName;
        setTimeout(() => this.deletedHero = '', 5000);
    }

    updatePaginator() {
        this.heroesSource.paginator = this.paginator;
    }
}
