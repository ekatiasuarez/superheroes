import { Component } from '@angular/core';
import { ProgressSpinnerMode } from '@angular/material/progress-spinner';
import { Subject } from 'rxjs';

import { LoaderService } from '../../services/loader.service';

@Component({
    selector: 'app-loader',
    templateUrl: './loader.component.html',
    styleUrls: ['./loader.component.sass']
})
export class LoaderComponent {

    public color: string = 'primary';
    public mode: ProgressSpinnerMode = 'indeterminate';
    public value = 0;
    
    public isLoading: Subject<boolean> = this.loaderService.isLoading;
    
    constructor(private loaderService: LoaderService) {
        // Need empty constructor for dependencies
    }
}
