import { Directive, HostListener, Input, OnInit } from '@angular/core';
import { AbstractControl } from '@angular/forms';

@Directive({
    selector: '[upperCaseControl]',
})
export class UpperCaseControlDirective implements OnInit {
    
    @Input() upperCaseControl: AbstractControl;
    
    constructor() {
        // Need empty constructor
    }
    ngOnInit(): void {
        this.toUpperCase();
    }
    
    @HostListener('input')
    toUpperCase() {
        this.upperCaseControl.setValue(this.upperCaseControl.value.toUpperCase());
    }
}
