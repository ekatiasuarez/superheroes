export class HeroModel {
    
    private _id: number;
    private _name: string;
    private _power: string;
    private _active: boolean;

    constructor(data: any) {
        this._id = data.id;
        this._name = data.name;
        this._power = data.power;
        this._active = !!data.active;
    }
    
    get id(): number {
        return this._id;
    }
    set id(value) {
        this._id = value;
    }
    
    get name(): string {
        return this._name;
    }
    set name(value) {
        this._name = value;
    }
    
    get power(): string {
        return this._power;
    }
    set power(value) {
        this._power = value;
    }
    
    get active(): boolean {
        return this._active;
    }
    set active(value) {
        this._active = value;
    }
}
