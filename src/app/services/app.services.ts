import { HeroService } from './hero.service';
import { HeroServiceMock } from './mocks/hero.service';

export const APP_SERVICES = [
    // Data services and their service mocks
    // Change useClass for use real API or mock data
    { provide: HeroService, useClass: HeroServiceMock }
]