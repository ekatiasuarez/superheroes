import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';

export abstract class CommonService {

    constructor(protected httpClient: HttpClient) {
        // Need empty constructor for dependencies
    }

    private _getHeaders() {
        let headers = new HttpHeaders();
        headers = headers.append('Content-Type', 'application/json');
        return headers;
    }

    public get(path: string): Promise<any> {
        return this.httpClient.get(`${environment.apiBaseUrl}${path}`, { headers: this._getHeaders() }).toPromise()
        .then((response) => response)
        .catch(error => Promise.reject(error));
    }

    public post(path: string, body: string): Promise<any> {
        return this.httpClient.post(`${environment.apiBaseUrl}${path}`, body, { headers: this._getHeaders() }).toPromise()
        .then((response) => response)
        .catch(error => Promise.reject(error));
    }

    public put(path: string, body: string): Promise<any> {
        return this.httpClient.put(`${environment.apiBaseUrl}${path}`, body, { headers: this._getHeaders() }).toPromise()
        .then((response) => response)
        .catch(error => Promise.reject(error));
    }

    public delete(path: string): Promise<any> {
        return this.httpClient.delete(`${environment.apiBaseUrl}${path}`, { headers: this._getHeaders() }).toPromise()
        .then((response) => response)
        .catch(error => Promise.reject(error));
    }
}