import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { HeroModel } from '../models/hero.model';
import { CommonService } from './common.service';

@Injectable()
export class HeroService extends CommonService {
    
    constructor(protected httpClient: HttpClient) {
        super(httpClient);
    }

    public getHeroes(): Promise<Array<HeroModel>> {
        const data: Array<HeroModel> = [];
        return new Promise((resolve, reject) => {
            this.get('/heroes').then(heroes => {
                if (heroes && heroes.length > 0) {
                    heroes.forEach((hero: any) => {
                        data.push(new HeroModel(hero));
                    });
                }
                resolve(data);
            }, error => reject(error));
        });
    }

    public getFilteredHeroes(term: string): Promise<Array<HeroModel>> {
        const data: Array<HeroModel> = [];
        return new Promise((resolve, reject) => {
            this.get(`/heroes/?name="${term}`).then(heroes => {
                if (heroes && heroes.length > 0) {
                    heroes.forEach((hero: any) => {
                        data.push(new HeroModel(hero));
                    });
                }
                resolve(data);
            }, error => reject(error));
        });
    }

    public getHeroById(heroId: number): Promise<HeroModel> {
        return new Promise((resolve, reject) => {
            this.get(`/heroes/${heroId}`).then(heroResult => {
                const hero = new HeroModel(heroResult);
                resolve(hero);
            }, error => reject(error));
        });
    }

    public createHero(data: any): Promise<boolean> {
        return new Promise((resolve, reject) => {
            this.post(`/heroes/create`, data).then(result => {
                resolve(result);
            }, error => reject(error));
        });
    }

    public updateHero(data: any, heroId: number): Promise<boolean> {
        return new Promise((resolve, reject) => {
            this.put(`/heroes/${heroId}/edit`, data).then(result => {
                resolve(result);
            }, error => reject(error));
        });
    }

    public deleteHero(heroId: number): Promise<boolean> {
        return new Promise((resolve, reject) => {
            this.delete(`/heroes/${heroId}`).then(result => {
                resolve(result);
            }, error => reject(error));
        });
    }
}