import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { HeroModel } from 'src/app/models/hero.model';

import { CommonService } from '../common.service';

@Injectable()
export class HeroServiceMock extends CommonService {

    constructor(protected httpClient: HttpClient) {
        super(httpClient);
    }

    public getHeroes(): Promise<Array<HeroModel>> {
        const data: Array<HeroModel> = []
        return new Promise((resolve, reject) => {
            this.get('heroes.json').then(heroes => {
                if (heroes && heroes.length > 0) {
                    heroes.forEach((hero: any) => {
                        data.push(new HeroModel(hero));
                    });
                }
                resolve(data);
            }, error => reject(error));
        });
    }

    public getFilteredHeroes(term: string): Promise<Array<HeroModel>> {
        const data: Array<HeroModel> = []
        return new Promise((resolve, reject) => {
            this.get('heroes.json').then(heroes => {
                heroes.forEach((hero: any) => {
                    if (hero.name.toLowerCase().includes(term.toLowerCase())) {
                        data.push(new HeroModel(hero));
                    }
                });
            });
            // Time out for simulate service request
            setTimeout(() => {
                resolve(data);
            }, 500);
        });
    }

    public getHeroById(heroId: number): Promise<HeroModel> {
        return new Promise((resolve, reject) => {
            this.get('heroes.json').then(heroes => {
                const data = heroes.find((heroResult: any) => heroResult.id == heroId);
                const hero: HeroModel = new HeroModel(data);
                // Time out for simulate service request
                setTimeout(() => {
                    resolve(hero);
                }, 500);
            });
        });
    }

    public createHero(data: any): Promise<boolean> {
        return this.simulateSuccessResponse();        
    }

    public updateHero(data: any, heroId: number): Promise<boolean> {
        return this.simulateSuccessResponse();
    }

    public deleteHero(heroId: number): Promise<boolean> {
        return this.simulateSuccessResponse();
    }

    private simulateSuccessResponse(): Promise<any> {
        return new Promise((resolve, reject) => {
            this.get('heroes.json').then(() => {
                // Time out for simulate service request
                setTimeout(() => {
                    resolve(true);
                }, 500);
            });
        });
    }
}